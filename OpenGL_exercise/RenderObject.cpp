#pragma once

#include "RenderObject.h"

#define FREEGLUT_STATIC

#include <gl/glew.h>

#define MAX_DEGREE 360.0f

RenderObject::RenderObject()
	: scale(VectorFloat3D(1.0, 1.0, 1.0))
{}


RenderObject::~RenderObject()
{}

void RenderObject::drawWithTransformations(const bool& scaleIndependet, const bool& rotateIndependet, const bool& translateIndependet)
{
	// not as nice as it could be :D

	glPushMatrix();
	
	doTranslation();
	doRotation();
	doScaling();
	draw();
	
	glPopMatrix();

	if (!translateIndependet)
		doTranslation();
	if (!rotateIndependet)
		doRotation();
	if (!scaleIndependet)
		doScaling();
}

void RenderObject::doTranslation()
{
	glTranslatef(position.x, position.y, position.z);
}

void RenderObject::doRotation()
{
	glRotatef(rotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(rotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(rotation.z, 0.0f, 0.0f, 1.0f);
}

void RenderObject::doScaling()
{
	glScalef(scale.x, scale.y, scale.z);
}

void RenderObject::move(const float& x, const float& y, const float& z)
{
	position.x += x;
	position.y += y;
	position.z += z;
}

void RenderObject::rotate(const float& x, const float& y, const float& z)
{
	rotation.x = fmod(rotation.x + x, MAX_DEGREE);
	rotation.y = fmod(rotation.y + y, MAX_DEGREE);
	rotation.z = fmod(rotation.z + z, MAX_DEGREE);
}
