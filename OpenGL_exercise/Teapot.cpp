#pragma once
#include "Teapot.h"

#define FREEGLUT_STATIC
#define _USE_MATH_DEFINES 

#include "gl/glew.h"
#include "glut.h"
#include <cmath>

TeapotScene::TeapotScene(float size)
	: curButtonPressed(0), teapot(TeapotObject(size))
{
	teapot.material.ambient = BLUE;
	teapot.material.diffuse = WHITE;
}

void TeapotScene::render()
{
	glPushMatrix();
	teapot.drawWithTransformations();
	glPopMatrix();
}

void TeapotScene::handleMouseKlick(const int& button, const int& state, const int& x, const int& y)
{
	if (button == GLUT_LEFT_BUTTON || button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_UP)
		{
			curButtonPressed = 0;
		}
		else
		{
			prevMousePos.x = x;
			prevMousePos.y = y;
			curButtonPressed = button;
		}
	}
}

void TeapotScene::handleMouseMotion(const int& x, const int& y)
{
	int deltaX = prevMousePos.x - x;
	int deltaY = prevMousePos.y - y;

	if (curButtonPressed == GLUT_LEFT_BUTTON)
	{
		rotate(deltaY / 2, -deltaX / 2, 0);
	}
	else if (curButtonPressed == GLUT_RIGHT_BUTTON)
	{
		move(float(deltaX*2) / glutGet(GLUT_WINDOW_WIDTH), float(deltaY*2) / glutGet(GLUT_WINDOW_HEIGHT), 0);
	}
	prevMousePos.x = x;
	prevMousePos.y = y;
}

void TeapotScene::rotate(float x, float y, float z)
{
	teapot.rotate(x, y, z);
}

void TeapotScene::move(float x, float y, float z)
{
	teapot.move(x, y, z);
}

TeapotObject::TeapotObject(float size)
{
	scale.x = size;
	scale.y = size;
	scale.z = size;
}

void TeapotObject::draw()
{
	glColor3f(BLUE.r, BLUE.g, BLUE.b);
	material.config();
	glutSolidTeapot(1.0f);
}