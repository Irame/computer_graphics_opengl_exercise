#pragma once
#include "Scene.h"
#include "Camera.h"

class Viewport
{
public:
	Viewport();
	~Viewport();

	ScenePtr scene;
	CameraPtr camera;

	float nearPlane;
	float farPlane;
	float fieldOfView;

	int width;
	int height;

	void setup();

	void render();
};

