#pragma once

#include "Viewport.h"

#define FREEGLUT_STATIC

#include <gl/glew.h>


Viewport::Viewport()
{}


Viewport::~Viewport()
{}

void Viewport::setup()
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float ar = static_cast<float>(width) / static_cast<float>(height);

	gluPerspective(fieldOfView, ar, nearPlane, farPlane);

	camera->enableLights();
	scene->enableLights();
}

void Viewport::render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	camera->renderLights();
	camera->callLookAt();

	scene->renderLights();
	scene->render();

	glFlush();
}