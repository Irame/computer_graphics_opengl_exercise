#pragma once
#include "Util.h"
#include <gl/glew.h>

class Material
{
public:
	Material(const GLenum& face = GL_FRONT);
	~Material();

	void config();

	GLenum face;
	float shininess;
	RGBAColor ambient;
	RGBAColor diffuse;
	RGBAColor specular;
	RGBAColor emission;
};

