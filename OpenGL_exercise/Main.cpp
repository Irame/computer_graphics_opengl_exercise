// Main.cpp
// The Simplest OpenGL program with GLUT

#define FREEGLUT_STATIC

#include <windows.h>		// Must have for Windows platform builds
#include "gl/glew.h"
#include "glut.h"			// Glut (Free-Glut on Windows)

#include <math.h>
#include <iostream>

#include "Teapot.h"
#include "Viewport.h"
#include "Robot.h"
#include "WavefrontObjParser.h"
#include "WavefrontObjScene.h"

class VertexRenderObject;
float cameraMoveDelta = 0.05;

CameraPtr camera;
ScenePtr scene;
Viewport viewport;

///////////////////////////////////////////////////////////
// Called to draw scene
void RenderScene(void)
{
	viewport.render();
}

///////////////////////////////////////////////////////////
// Setup the rendering state
void SetupRC(void)
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}


void TimerFunction(int value)
{

	glutPostRedisplay();
	glutTimerFunc(33, TimerFunction, 1);
}

void ChangeSize(GLsizei width, GLsizei height)
{
	viewport.width = width;
	viewport.height = height;

	viewport.setup();
}

void KeyPress(unsigned char key, int x, int y)
{
	scene->handleKeyPress(key, x, y);
	if (key == 27)
	{
		exit(0);
	}
}

void SpecialKeyPress(int key, int x, int y)
{
	scene->handleSpecialKeyPress(key, x, y);
}

void MouseClick(int button, int state, int x, int y)
{
	scene->handleMouseKlick(button, state, x, y);

	if (button == 3)
	{
		camera->move(0, 0, cameraMoveDelta);
	}
	else if (button == 4)
	{
		camera->move(0, 0, -cameraMoveDelta);
	}
}

void MouseMotion(int x, int y)
{
	scene->handleMouseMotion(x, y);
}

///////////////////////////////////////////////////////////
// Main program entry point
int main(int argc, char* argv[])
{
	viewport.nearPlane = 1.0f;
	viewport.farPlane = 400.0f;
	viewport.fieldOfView = 60.0f;

	camera = std::make_shared<Camera>();
	camera->position = VectorFloat3D(0.0f, 0.0f, -50.0f);
	camera->upVector = VectorFloat3D(0.0f, 1.0f, 0.0f);
	camera->lookAtPoint = VectorFloat3D(0.0f, 0.0f, 0.0f);

	// sheet 7, task 1
	//scene = std::make_shared<TeapotScene>(0.5);

	// sheet 7, task 2
	//scene = std::make_shared<RobotScene>();

	scene = std::make_shared<WavefrontObjScene>("C:/Users/Felix/Downloads/dinosaur_obj/dinosaur obj/dinosaur.obj");
	
	//scene = std::make_shared<WavefrontObjScene>("C:/Users/Felix/Downloads/melon_obj/melon.obj");

	LightPtr light0 = std::make_shared<Light>(GL_LIGHT0);
	light0->position = VectorFloat3D(6, 0, -10);
	light0->ambientColor = WHITE.changeColorIntensity(0.5);
	light0->diffuseColor = WHITE;
	camera->lights.push_back(light0);

	LightPtr light1 = std::make_shared<Light>(GL_LIGHT1);
	light1->position = VectorFloat3D(-6, 0, -10);
	light1->ambientColor = WHITE.changeColorIntensity(0.5);
	light1->diffuseColor = WHITE;
	camera->lights.push_back(light1);

	viewport.camera = camera;
	viewport.scene = scene;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
 	glutCreateWindow("OpenGL_exercise");
	glutFullScreen();
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glShadeModel(GL_SMOOTH);

	glutDisplayFunc(RenderScene);
	glutKeyboardFunc(KeyPress);
	glutSpecialFunc(SpecialKeyPress);
	glutMouseFunc(MouseClick);
	glutMotionFunc(MouseMotion);
	glutReshapeFunc(ChangeSize);

	glutTimerFunc(33, TimerFunction, 1);

	SetupRC();

	glutMainLoop();
    
    return 0;
}