#pragma once

#include "VertexRenderObject.h"
#include <gl/glew.h>
#include <vector>
#include "Util.h"
#include <algorithm>

VertexRenderObject::VertexRenderObject(const GLenum& mode)
	: mode(mode)
{}


VertexRenderObject::~VertexRenderObject()
{}

void VertexRenderObject::draw()
{
	material.config();
	glBegin(mode);
	for (auto& vertex : vertices)
	{
		glNormal3f(vertex.normal.x, vertex.normal.y, vertex.normal.z);
		glVertex3f(vertex.point.x, vertex.point.y, vertex.point.z);
	}
	glEnd();
}

void VertexRenderObject::calcNormals()
{
	std::vector<Vertex>::iterator vertexIter = vertices.begin();
	switch (mode)
	{
	case GL_TRIANGLE_STRIP: {
		if (vertices.size() < 3) return;
		Vertex& vertex0 = *vertexIter++;
		Vertex& vertex1 = *vertexIter++;
		for (; vertexIter != vertices.end(); ++vertexIter)
		{
			Vertex& vertex2 = *vertexIter;
			vertex2.normal = (vertex0.point - vertex2.point).crossProduct(vertex1.point - vertex2.point);
			vertex2.normal /= vertex2.normal.abs();
			vertex0 = vertex1;
			vertex1 = vertex2;
		}
	} break;
	case GL_TRIANGLES:
		for (; vertexIter != vertices.end(); )
		{
			Vertex& vertex0 = *vertexIter++;
			Vertex& vertex1 = *vertexIter++;
			Vertex& vertex2 = *vertexIter++;
			vertex0.normal = (vertex2.point - vertex0.point).crossProduct(vertex1.point - vertex0.point);
			vertex0.normal /= vertex0.normal.abs();
			vertex1.normal = vertex2.normal = vertex0.normal;
		}
		break;
	}
}

void VertexRenderObject::normalizePosition(VectorFloat3D offset)
{
	VectorFloat3D minPoint;
	VectorFloat3D maxPoint;

	for (auto& vec : vertices)
	{
		if (vec.point.x < minPoint.x) minPoint.x = vec.point.x;
		if (vec.point.y < minPoint.y) minPoint.y = vec.point.y;
		if (vec.point.z < minPoint.z) minPoint.z = vec.point.z;

		if (vec.point.x > maxPoint.x) maxPoint.x = vec.point.x;
		if (vec.point.y > maxPoint.y) maxPoint.y = vec.point.y;
		if (vec.point.z > maxPoint.z) maxPoint.z = vec.point.z;
	}

	position = -((maxPoint - minPoint) / 2 + minPoint);
}

void VertexRenderObject::normalizeSize(float size)
{
	VectorFloat3D minPoint;
	VectorFloat3D maxPoint;
	
	for (auto& vec : vertices)
	{
		if (vec.point.x < minPoint.x) minPoint.x = vec.point.x;
		if (vec.point.y < minPoint.y) minPoint.y = vec.point.y;
		if (vec.point.z < minPoint.z) minPoint.z = vec.point.z;

		if (vec.point.x > maxPoint.x) maxPoint.x = vec.point.x;
		if (vec.point.y > maxPoint.y) maxPoint.y = vec.point.y;
		if (vec.point.z > maxPoint.z) maxPoint.z = vec.point.z;
	}

	VectorFloat3D result = maxPoint - minPoint;
	float maxCoord = std::max(result.x, std::max(result.y, result.z));
	float normalizedSize = size / maxCoord;
	scale = VectorFloat3D(normalizedSize, normalizedSize, normalizedSize);
}