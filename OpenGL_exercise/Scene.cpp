#pragma once

#include "Scene.h"


Scene::Scene()
{
}


Scene::~Scene()
{
}

void Scene::renderLights()
{
	for (auto light : lights)
		light->render();
}

void Scene::enableLights()
{
	for (auto light : lights)
		light->enable();
}