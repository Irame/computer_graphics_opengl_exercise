#pragma once
#include "Scene.h"
#include "WavefrontObjParser.h"

class WavefrontObjScene
	: public Scene
{
public:
	WavefrontObjScene(const std::string& fileName);
	~WavefrontObjScene();

	void render() override;

	void handleMouseKlick(const int& button, const int& state, const int& x, const int& y) override;
	void handleMouseMotion(const int& x, const int& y) override;

private:
	float rotateDelta;

	int curButtonPressed;
	MousePos prevMousePos;

	VertexRenderObjectPtr renderObject;
};

