#pragma once

#include "Light.h"


#define FREEGLUT_STATIC
#define _USE_MATH_DEFINES 

#include "gl/glew.h"
#include "glut.h"

Light::Light(const GLenum& lightNum)
	: active(true), lightNum(lightNum)
{
}


Light::~Light()
{
}

void Light::enable()
{
	if (active)
		glEnable(lightNum);
	else
		glDisable(lightNum);
}

void Light::render()
{
	visualisation.position = position;
	visualisation.drawWithTransformations(true, true, true);
	glLightfv(lightNum, GL_AMBIENT, ambientColor.getArray());
	glLightfv(lightNum, GL_DIFFUSE, diffuseColor.getArray());
	glLightfv(lightNum, GL_SPECULAR, specularColor.getArray());
	glLightfv(lightNum, GL_POSITION, position.getArray());
}

VisualLight::VisualLight()
{
	material.emission = WHITE;
}

void VisualLight::draw()
{
	material.config();
	glColor3fv(WHITE.getArray());
	glutSolidSphere(1.0f, 10, 10);
}