#pragma once

#include <cmath>

struct VectorFloat3D
{
	float x, y, z, w;

	VectorFloat3D(const float& x = 0.0f, const float& y = 0.0f, const float& z = 0.0f, const float& w = 1.0f)
		: x(x), y(y), z(z), w(w), arrayRepresentation(new float[4]) {}

	float* getArray()
	{
		arrayRepresentation[0] = x;
		arrayRepresentation[1] = y;
		arrayRepresentation[2] = z;
		arrayRepresentation[3] = w;
		return arrayRepresentation;
	}

	VectorFloat3D crossProduct(const VectorFloat3D& other) const
	{
		return (x*other.y - other.x*y, y*other.z - other.y*z, z*other.x - x*other.z);
	}

	VectorFloat3D&  operator-=(const VectorFloat3D& other) {
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}

	float abs()
	{
		return sqrtf(powf(x, 2) + powf(y, 2) + powf(z, 2));
	}

	VectorFloat3D& operator/=(const float& f)
	{
		x /= f;
		y /= f;
		z /= f;
		return *this;
	}

	VectorFloat3D& operator+=(const VectorFloat3D& other) {
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	};

	VectorFloat3D operator-() {
		return(-x, -y, -z);
	}
private:
	float* arrayRepresentation;
};


inline VectorFloat3D operator/(const VectorFloat3D& vec, const float& f) {
	return (vec.x / f, vec.y / f, vec.z / f);
}


inline VectorFloat3D operator+(const VectorFloat3D& lhs, const VectorFloat3D& rhs) {
	return (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}

inline VectorFloat3D operator-(VectorFloat3D other1, const VectorFloat3D& other2) {
	other1 -= other2;
	return other1;
}


struct RGBAColor
{
	float r, g, b, a;
	RGBAColor(const float& r = 0.0f, const float& g = 0.0f, const float& b = 0.0f, const float& a = 1.0f) :
		r(r), g(g), b(b), a(a), arrayRepresentation(new float[4]) {}

	float* getArray()
	{
		arrayRepresentation[0] = r;
		arrayRepresentation[1] = g;
		arrayRepresentation[2] = b;
		arrayRepresentation[3] = a;
		return arrayRepresentation;
	}

	RGBAColor changeColorIntensity(const float& factor) const
	{
		RGBAColor result = *this;
		result.r *= factor;
		result.g *= factor;
		result.b *= factor;
		return result;
	}

private:
	float* arrayRepresentation;
};

static RGBAColor WHITE(1, 1, 1);
static RGBAColor RED(1, 0, 0);
static RGBAColor GREEN(0, 1, 0);
static RGBAColor BLUE(0, 0, 1);
static RGBAColor YELLOW(1, 1, 0);