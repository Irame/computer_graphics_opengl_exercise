#pragma once
#include "Scene.h"

class CubeObject
	: public RenderObject
{
public:
	CubeObject();
	CubeObject(RGBAColor color, VectorFloat3D size, VectorFloat3D offset, VectorFloat3D rotation);

	void draw() override;

private:
	RGBAColor color;
};

class RobotScene
	: public Scene
{
public:
	RobotScene();


	void handleKeyPress(const unsigned char& key, const int& x, const int& y) override;
	void handleMouseKlick(const int& button, const int& state, const int& x, const int& y) override;
	void handleMouseMotion(const int& x, const int& y) override;

	void render() override;

private:
	float rotateDelta;

	int curButtonPressed;
	MousePos prevMousePos;
	VectorFloat3D rotation;

	CubeObject body;

	CubeObject upperArm1;
	CubeObject lowerArm1;
	CubeObject hand1;

	CubeObject upperArm2;
	CubeObject lowerArm2;
	CubeObject hand2;
};
