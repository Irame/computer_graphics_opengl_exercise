#pragma once
#include <memory>
#include "Util.h"
#include "Material.h"


class RenderObject
{
public:
	RenderObject();
	~RenderObject();

	VectorFloat3D scale;
	VectorFloat3D position;
	VectorFloat3D rotation;
	Material  material;

	void drawWithTransformations(const bool& scaleIndependet = true, const bool& rotationIndependet = false, const bool& translationIndependet = false);

	void doTranslation();
	void doRotation();
	void doScaling();

	virtual void draw() = 0;

	void move(const float& x, const float& y, const float& z);
	void rotate(const float& x, const float& y, const float& z);
};

typedef std::shared_ptr<RenderObject> RenderObjectPtr;