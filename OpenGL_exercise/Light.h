#pragma once
#include "Util.h"
#include "gl/glew.h"
#include <memory>
#include "RenderObject.h"

class VisualLight
	: public RenderObject
{
public:
	VisualLight();

	void draw() override;
};

class Light
{
public:
	Light(const GLenum& lightNum);
	~Light();

	void enable();
	void render();

	bool active;
	GLenum lightNum;
	VectorFloat3D position;
	RGBAColor ambientColor;
	RGBAColor diffuseColor;
	RGBAColor specularColor;

	VisualLight visualisation;
};

typedef std::shared_ptr<Light> LightPtr;