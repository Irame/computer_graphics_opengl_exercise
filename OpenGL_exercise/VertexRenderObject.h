#pragma once

#include "Util.h"
#include "RenderObject.h"
#include <vector>


struct Vertex
{
	Vertex(VectorFloat3D point, VectorFloat3D texCoords, VectorFloat3D normal)
		: point(point), texCoords(texCoords), normal(normal) {}

	VectorFloat3D point;
	VectorFloat3D texCoords;
	VectorFloat3D normal;
};

class VertexRenderObject
	: public RenderObject
{
public:
	VertexRenderObject(const GLenum& mode = GL_TRIANGLE_STRIP);
	~VertexRenderObject();

	GLenum mode;
	std::vector<Vertex> vertices;

	void draw() override;
	void calcNormals();

	void normalizePosition(VectorFloat3D offset = VectorFloat3D());
	void normalizeSize(float size = 1.0f);
};

typedef std::shared_ptr<VertexRenderObject> VertexRenderObjectPtr;