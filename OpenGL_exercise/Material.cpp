#pragma once

#include "Material.h"
#include <gl/glew.h>


Material::Material(const GLenum& face)
	: face(face), shininess(0.5)
{
}


Material::~Material()
{
}

void Material::config()
{
	glMaterialfv(face, GL_AMBIENT, ambient.getArray());
	glMaterialfv(face, GL_DIFFUSE, diffuse.getArray());
	glMaterialfv(face, GL_SPECULAR, specular.getArray());
	glMaterialfv(face, GL_EMISSION, emission.getArray());
	glMaterialfv(face, GL_SHININESS, &shininess);
}