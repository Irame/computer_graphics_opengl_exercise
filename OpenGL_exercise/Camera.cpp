#pragma once

#include "Camera.h"

#define FREEGLUT_STATIC

#include "glut.h"

Camera::Camera()
{
}


Camera::~Camera()
{
}

void Camera::callLookAt()
{
	gluLookAt(
		position.x, position.y, position.z, 
		lookAtPoint.x, lookAtPoint.y, lookAtPoint.z,
		upVector.x, upVector.y, upVector.z);
}

void Camera::renderLights()
{
	for (auto light : lights)
		light->render();
}

void Camera::enableLights()
{
	for (auto light : lights)
		light->enable();
}

void Camera::move(const float& x, const float& y, const float& z, const bool& moveLookAt)
{
	position.x += x;
	position.y += y;
	position.z += z;

	if (moveLookAt)
	{
		lookAtPoint.x += x;
		lookAtPoint.y += y;
		lookAtPoint.z += z;
	}
}