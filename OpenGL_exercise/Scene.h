#pragma once
#include <vector>
#include "Light.h"

struct MousePos { int x = 0, y = 0; };

class Scene
{
public:
	Scene();
	~Scene();

	virtual void render() = 0;

	void renderLights();
	void enableLights();

	virtual void handleKeyPress(const unsigned char& key, const int& x, const int& y) {};
	virtual void handleSpecialKeyPress(const int& key, const int& x, const int& y) {};
	virtual void handleMouseKlick(const int& button, const int& state, const int& x, const int& y) {};
	virtual void handleMouseMotion(const int& x, const int& y) {};

	std::vector<LightPtr> lights;
};

typedef std::shared_ptr<Scene> ScenePtr;
