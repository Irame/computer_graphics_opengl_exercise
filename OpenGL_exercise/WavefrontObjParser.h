#pragma once
#include <string>
#include "VertexRenderObject.h"

struct Face
{
	int data[3][3];
	Face() {}
	int* operator[](int idx)
	{
		return data[idx];
	}
	void reset()
	{
		memset(data, 0, sizeof(int) * 9);
	}
};

typedef std::shared_ptr<std::vector<std::string>> TokensPtr;

class WavefrontObjParser
{
	std::vector<VectorFloat3D> points;
	std::vector<VectorFloat3D> texCoords;
	std::vector<VectorFloat3D> normals;
	std::vector<Face> faceses;

public:
	WavefrontObjParser(const std::string& fileName);

	void parseFile();

	VertexRenderObjectPtr generateRenderObject();
private:
	std::string fileName;
	TokensPtr lineTokens;
	TokensPtr faceTokens;
	Face dummyFace;

	void parseVertex(TokensPtr tokens);
	void parseFace(TokensPtr tokens);
	void parseTexCoord(TokensPtr tokens);
	void parseNormal(TokensPtr tokens);

	static void tokenize(const std::string& str, const std::string& delimiter, TokensPtr buffer);
};
