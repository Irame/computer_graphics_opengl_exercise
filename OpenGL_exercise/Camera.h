#pragma once
#include "Util.h"
#include "Light.h"
#include <vector>


class Camera
{
public:
	Camera();
	~Camera();

	void callLookAt();

	void renderLights();
	void enableLights();

	void move(const float& x, const float& y, const float& z, const bool& moveLookAt = true);

	VectorFloat3D position;
	VectorFloat3D upVector;
	VectorFloat3D lookAtPoint;

	std::vector<LightPtr> lights;
};

typedef std::shared_ptr<Camera> CameraPtr;