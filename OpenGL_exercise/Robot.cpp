#pragma once
#include "Robot.h"

#define FREEGLUT_STATIC
#define _USE_MATH_DEFINES 

#include "gl/glew.h"
#include "glut.h"

CubeObject::CubeObject()
{}

CubeObject::CubeObject(RGBAColor color, VectorFloat3D size, VectorFloat3D rotation, VectorFloat3D offset)
{
	this->scale = size;
	this->rotation = rotation;
	this->position = offset;
	this->color = color;
}

void CubeObject::draw()
{
	glColor3f(color.r, color.g, color.b);
	material.config();
	glutSolidCube(1.0f);
}

// not nice ... a lot of magic numbers
RobotScene::RobotScene() :
	rotateDelta(10),
	body(BLUE, VectorFloat3D(2, 1, 1), VectorFloat3D(0, 0, 0), VectorFloat3D(0, 0, 0)),
	upperArm1(GREEN, VectorFloat3D(0.5f, 1.0f, 0.5f), VectorFloat3D(0.0f, 0.0f, 0.0f), VectorFloat3D(0.5f, 1.0f, 0.0f)),
	lowerArm1(RED, VectorFloat3D(1.5f, 0.5f, 0.5f), VectorFloat3D(0.0f, 0.0f, 0.0f), VectorFloat3D(0.5f, 0.75f, 0.0f)),
	hand1(YELLOW, VectorFloat3D(0.25f, 1.0f, 0.25f), VectorFloat3D(0.0f, 0.0f, 0.0f), VectorFloat3D(0.8625f, 0.0f, 0.0f)),
	upperArm2(GREEN, VectorFloat3D(0.5f, 1.0f, 0.5f), VectorFloat3D(0.0f, 0.0f, 0.0f), VectorFloat3D(-0.5f, 1.0f, 0.0f)),
	lowerArm2(RED, VectorFloat3D(1.5f, 0.5f, 0.5f), VectorFloat3D(0.0f, 0.0f, 0.0f), VectorFloat3D(-0.5f, 0.75f, 0.0f)),
	hand2(YELLOW, VectorFloat3D(0.25f, 1.0f, 0.25f), VectorFloat3D(0.0f, 0.0f, 0.0f), VectorFloat3D(-0.8625f, 0.0f, 0.0f))
{
	body.material.ambient = BLUE.changeColorIntensity(0.5);
	body.material.diffuse = BLUE;

	upperArm1.material.ambient = GREEN.changeColorIntensity(0.5);
	upperArm1.material.diffuse = GREEN;
	upperArm2.material.ambient = GREEN.changeColorIntensity(0.5);
	upperArm2.material.diffuse = GREEN;

	lowerArm1.material.ambient = RED.changeColorIntensity(0.5);
	lowerArm1.material.diffuse = RED;
	lowerArm2.material.ambient = RED.changeColorIntensity(0.5);
	lowerArm2.material.diffuse = RED;

	hand1.material.ambient = YELLOW.changeColorIntensity(0.5);
	hand1.material.diffuse = YELLOW;
	hand2.material.ambient = YELLOW.changeColorIntensity(0.5);
	hand2.material.diffuse = YELLOW;
}

void RobotScene::handleKeyPress(const unsigned char& key, const int& x, const int& y)
{
	switch (key)
	{
	case 'a': upperArm1.rotate(0.0, rotateDelta, 0.0); break;
	case 'd': upperArm1.rotate(0.0, -rotateDelta, 0.0); break;
	case 'w': hand1.rotate(rotateDelta, 0.0, 0.0); break;
	case 's': hand1.rotate(-rotateDelta, 0.0, 0.0); break;

	case '4': upperArm2.rotate(0.0, rotateDelta, 0.0); break;
	case '6': upperArm2.rotate(0.0, -rotateDelta, 0.0); break;
	case '8': hand2.rotate(rotateDelta, 0.0, 0.0); break;
	case '5': hand2.rotate(-rotateDelta, 0.0, 0.0); break;
	}
}

void RobotScene::handleMouseKlick(const int& button, const int& state, const int& x, const int& y)
{
	if (button == GLUT_LEFT_BUTTON || button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_UP)
		{
			curButtonPressed = 0;
		}
		else
		{
			prevMousePos.x = x;
			prevMousePos.y = y;
			curButtonPressed = button;
		}
	}
}

void RobotScene::handleMouseMotion(const int& x, const int& y)
{
	int deltaX = prevMousePos.x - x;
	int deltaY = prevMousePos.y - y;

	if (curButtonPressed == GLUT_LEFT_BUTTON)
	{
		body.rotate(deltaY / 2, -deltaX / 2, 0);
	}
	else if (curButtonPressed == GLUT_RIGHT_BUTTON)
	{
		body.move(float(deltaX) / glutGet(GLUT_WINDOW_WIDTH), float(deltaY) / glutGet(GLUT_WINDOW_HEIGHT), 0);
	}
	prevMousePos.x = x;
	prevMousePos.y = y;
}

// not nice ... could be much nicer with a loop
void RobotScene::render()
{
	// === start
	glPushMatrix();

	// === base
	body.drawWithTransformations();

	// === branch 1
	glPushMatrix();

	// === upperArm1
	upperArm1.drawWithTransformations();

	// === lowerArm1
	lowerArm1.drawWithTransformations();

	// === hand1
	hand1.drawWithTransformations();


	// === branch 1 end
	glPopMatrix();


	// === branch 2
	glPushMatrix();

	// === upperArm2
	upperArm2.drawWithTransformations();

	// === lowerArm2
	lowerArm2.drawWithTransformations();

	// === hand2
	hand2.drawWithTransformations();

	// === branch 2 end
	glPopMatrix();

	// === end
	glPopMatrix();
}