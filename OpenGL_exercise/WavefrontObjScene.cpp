#include "WavefrontObjScene.h"

#define FREEGLUT_STATIC
#define _USE_MATH_DEFINES 

#include "gl/glew.h"
#include "glut.h"

WavefrontObjScene::WavefrontObjScene(const std::string& fileName)
{
	WavefrontObjParser parser(fileName);
	parser.parseFile();
	renderObject = parser.generateRenderObject();
	renderObject->calcNormals();
	renderObject->normalizePosition();
	renderObject->normalizeSize(20);
	renderObject->material.diffuse = YELLOW;
	renderObject->material.ambient = RED;
}

WavefrontObjScene::~WavefrontObjScene()
{
}

void WavefrontObjScene::render()
{
	renderObject->drawWithTransformations();
}

void WavefrontObjScene::handleMouseKlick(const int& button, const int& state, const int& x, const int& y)
{
	if (button == GLUT_LEFT_BUTTON || button == GLUT_RIGHT_BUTTON)
	{
		if (state == GLUT_UP)
		{
			curButtonPressed = -1;
		}
		else
		{
			prevMousePos.x = x;
			prevMousePos.y = y;
			curButtonPressed = button;
		}
	}
}

void WavefrontObjScene::handleMouseMotion(const int& x, const int& y)
{
	int deltaX = prevMousePos.x - x;
	int deltaY = prevMousePos.y - y;

	if (curButtonPressed == GLUT_LEFT_BUTTON)
	{
		renderObject->rotate(deltaY / 2, -deltaX / 2, 0);
	}
	else if (curButtonPressed == GLUT_RIGHT_BUTTON)
	{
		renderObject->move(float(deltaX * 10) / glutGet(GLUT_WINDOW_WIDTH), float(deltaY * 10) / glutGet(GLUT_WINDOW_HEIGHT), 0);
	}
	prevMousePos.x = x;
	prevMousePos.y = y;
}