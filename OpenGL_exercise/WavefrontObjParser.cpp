#pragma once

#include "WavefrontObjParser.h"
#include <iostream>
#include <fstream>
#include <memory>

const char lineDelimiter = ' ';
const std::string lineDelimiterStr = " ";
const std::string facePartDelimiterStr = "/";

WavefrontObjParser::WavefrontObjParser(const std::string& fileName)
	: fileName(fileName)
{
	faceTokens = std::make_shared<std::vector<std::string>>();
	lineTokens = std::make_shared<std::vector<std::string>>();
}

void WavefrontObjParser::parseFile()
{
	std::string line;
	std::ifstream myfile(fileName);


	int linecount = 0;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			linecount++;
			tokenize(line, lineDelimiterStr, lineTokens);
			if (lineTokens->empty()) continue;
			std::string& key = lineTokens->at(0);
			if (key == "v")
			{
				parseVertex(lineTokens);
			}
			else if (key == "f")
			{
				parseFace(lineTokens);
			}
			else if (key == "vt") 
			{
				parseTexCoord(lineTokens);
			}
			else if (key == "vn")
			{
				parseNormal(lineTokens);
			}
			if (linecount % 100000 == 0)
				std::cout << "processed lines: " << (linecount/1000) << "k" << std::endl;
		}
		myfile.close();
	}
}

VertexRenderObjectPtr WavefrontObjParser::generateRenderObject()
{
	VertexRenderObjectPtr result = std::make_shared<VertexRenderObject>(GL_TRIANGLES);
	auto& verticies = result->vertices;
	
	for (auto& face : faceses)
	{
		for (int idx = 0; idx < 3; ++idx)
		{
			verticies.emplace_back(
				points.empty() ? 0.0f : points[face.data[idx][0] - 1], 
				texCoords.empty() ? 0.0f : texCoords[face.data[idx][1] - 1],
				normals.empty() ? 0.0f : normals[face.data[idx][2] - 1]);
		}
	}
	return result;
}

void WavefrontObjParser::parseVertex(TokensPtr tokens)
{
	points.emplace_back(
		stof(tokens->at(1)), 
		stof(tokens->at(2)),
		stof(tokens->at(3)),
		tokens->size() > 4 ? stof(tokens->at(4)) : 0.0f);
}

void WavefrontObjParser::parseFace(TokensPtr tokens)
{
	dummyFace.reset();
	int j;
	for (int i = 1; i < tokens->size(); i++)
	{
		j = 0;
		tokenize(tokens->at(i), facePartDelimiterStr, faceTokens);
		for (std::string& subPart : *faceTokens)
		{
			dummyFace[i - 1][j++] = stoi(subPart);
		}
	}
	faceses.push_back(dummyFace);
}

void WavefrontObjParser::parseTexCoord(TokensPtr tokens)
{
	texCoords.emplace_back(
		stof(tokens->at(1)),
		stof(tokens->at(2)),
		tokens->size() > 3 ? stof(tokens->at(3)) : 0.0f);
}

void WavefrontObjParser::parseNormal(TokensPtr tokens)
{
	normals.emplace_back(
		stof(tokens->at(1)),
		stof(tokens->at(2)),
		stof(tokens->at(3)));
}

void WavefrontObjParser::tokenize(const std::string& str, const std::string& delimiter, TokensPtr buffer)
{
	buffer->clear();
	size_t prevPos = 0;
	for (size_t curPos = str.find(delimiter, prevPos); curPos != std::string::npos; curPos = str.find(delimiter, prevPos))
	{
		buffer->push_back(str.substr(prevPos, curPos - prevPos));
		prevPos = curPos + delimiter.length();;
	}
	if (!str.empty()) 
		buffer->push_back(str.substr(prevPos, str.length() - prevPos));
}