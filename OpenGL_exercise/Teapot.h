#pragma once
#include "Scene.h"

class TeapotObject :
	public RenderObject
{

public:
	TeapotObject(float size);
	
	void draw() override;
};

class TeapotScene :
	public Scene
{
public:
	TeapotScene(float size);

	void render() override;

	void handleMouseKlick(const int& button, const int& state, const int& x, const int& y) override;
	void handleMouseMotion(const int& x, const int& y) override;

	void rotate(float x, float y, float z);
	void move(float x, float y, float z);

private:
	int curButtonPressed;
	MousePos prevMousePos;
	TeapotObject teapot;
};
